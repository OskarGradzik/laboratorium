﻿using System;
using System.Diagnostics.Metrics;

class zad2_Ankieta
{
    static void Main()
    {
        int rpg = 0;
        int shooter = 0;
        int mmo = 0;
        int survival = 0;
        string odp;

        Console.WriteLine("1.Jaki wyraz najbardziej Ciebie opisuje?");
        Console.WriteLine("a. Realizm");
        Console.WriteLine("b. Akcja");
        Console.WriteLine("c. Przyjaciele");
        Console.WriteLine("d. Przetrwanie");
        do
        {
            odp = Console.ReadLine();
            if (odp != "a" && odp != "b" && odp != "c" && odp != "d")
            {
                Console.WriteLine("Twoja odpowiedź była inna niż a,b,c lub d! Spróbuj jeszcze raz:");
            }
        } while (odp != "a" && odp != "b" && odp != "c" && odp != "d");
        if (odp == "a") rpg++;
        if (odp == "b") shooter++;
        if (odp == "c") mmo++;
        if (odp == "d") survival++;

        Console.WriteLine("2.Co zabrałbyś na bezludną wyspę?");
        Console.WriteLine("a. Jedzenie");
        Console.WriteLine("b. Karbin");
        Console.WriteLine("c. Przyjaciół");
        Console.WriteLine("d. Coś do rozpalenie ogniska");
        do
        {
            odp = Console.ReadLine();
            if (odp != "a" && odp != "b" && odp != "c" && odp != "d")
            {
                Console.WriteLine("Twoja odpowiedź była inna niż a,b,c lub d! Spróbuj jeszcze raz:");
            }
        } while (odp != "a" && odp != "b" && odp != "c" && odp != "d");
        if (odp == "a") rpg++;
        if (odp == "b") shooter++;
        if (odp == "c") mmo++;
        if (odp == "d") survival++;

        Console.WriteLine("3.Którą z wymienionych postaci lubisz najbardziej?");
        Console.WriteLine("a. Geralta z Rivii");
        Console.WriteLine("b. Snipera");
        Console.WriteLine("c. Gengara");
        Console.WriteLine("d. Steave");
        do
        {
            odp = Console.ReadLine();
            if (odp != "a" && odp != "b" && odp != "c" && odp != "d")
            {
                Console.WriteLine("Twoja odpowiedź była inna niż a,b,c lub d! Spróbuj jeszcze raz:");
            }
        } while (odp != "a" && odp != "b" && odp != "c" && odp != "d");
        if (odp == "a") rpg++;
        if (odp == "b") shooter++;
        if (odp == "c") mmo++;
        if (odp == "d") survival++;

        Console.WriteLine("4.Który z wymienionych streamerow lubisz najbardziej?");
        Console.WriteLine("a. Rojo");
        Console.WriteLine("b. Izak");
        Console.WriteLine("c. Rock");
        Console.WriteLine("d. Pagu");
        do
        {
            odp = Console.ReadLine();
            if (odp != "a" && odp != "b" && odp != "c" && odp != "d")
            {
                Console.WriteLine("Twoja odpowiedź była inna niż a,b,c lub d! Spróbuj jeszcze raz:");
            }
        } while (odp != "a" && odp != "b" && odp != "c" && odp != "d");
        if (odp == "a") rpg++;
        if (odp == "b") shooter++;
        if (odp == "c") mmo++;
        if (odp == "d") survival++;

        Console.WriteLine("5.Jaki jest twój ulubiony kolor?");
        Console.WriteLine("a. Zielony");
        Console.WriteLine("b. czerwony");
        Console.WriteLine("c. szary");
        Console.WriteLine("d. czarny");
        do
        {
            odp = Console.ReadLine();
            if (odp != "a" && odp != "b" && odp != "c" && odp != "d")
            {
                Console.WriteLine("Twoja odpowiedź była inna niż a,b,c lub d! Spróbuj jeszcze raz:");
            }
        } while (odp != "a" && odp != "b" && odp != "c" && odp != "d");
        if (odp == "a") rpg++;
        if (odp == "b") shooter++;
        if (odp == "c") mmo++;
        if (odp == "d") survival++;

        if (rpg >= survival && rpg >= shooter && rpg >= mmo)
        {
            Console.WriteLine("Twoim ulubionym gatunkiem jest rpg");
            Console.WriteLine("Ilość zdobytych punktów......" + rpg);
        }
        if (shooter >= survival && shooter >= rpg && shooter >= mmo)
        {
            Console.WriteLine("Twoim ulubionym gatunkiem jest shooter");
            Console.WriteLine("Ilość zdobytych punktów......" + shooter);
        }
        if (mmo >= survival && mmo >= shooter && mmo >= rpg)
        {
            Console.WriteLine("Twoim ulubionym gatunkiem jest mmo");
            Console.WriteLine("Ilość zdobytych punktów......" + mmo);
        }
        if (survival >= rpg && survival >= shooter && survival >= mmo)
        {
            Console.WriteLine("Twoim ulubionym gatunkiem jest survival");
            Console.WriteLine("Ilość zdobytych punktów......" + survival);
        }
        Console.Read();
    }
}